#include <SoftwareSerial.h>

#define LED_PIN     13

static boolean light_led = false;
static unsigned long nextSwitchTime = millis() + 300000;

int leftFRONT  = 5;
int leftREAR   = 6;
int rigthFRONT = 9;
int rightREAR  = 10;

int i = 0;         // variable to store the read value
char serialString;
int speedSET = 0;
int speedLeft = 0;
int speedRight = 0;

SoftwareSerial mySerial(0, 1); // RX, TX

void setup() {
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  mySerial.begin(9600);

  pinMode(leftFRONT, OUTPUT);
  pinMode(leftREAR, OUTPUT);
  pinMode(rigthFRONT, OUTPUT);
  pinMode(rightREAR, OUTPUT);
}

void loop()
{
  // run over and over
  if (mySerial.available()) 
  {
    delay(1);
    serialString = mySerial.read();

    /*move forward - receive a F and next two commands are left, then right speed settings for the PWM.
      All calculation for speed must be made on remote controler,
    */
    
    if (serialString == 'F')
    {
      //while(mySerial.available()){mySerial.read();}

      while (!mySerial.available()) 
      {
        delay(1);
      }
      
      delay(1);
      speedLeft = mySerial.read();

      //while(mySerial.available()){mySerial.read();}

      while (!mySerial.available()) 
      {
        delay(1);
      }
      delay(1);
      speedRight = mySerial.read();

      digitalWrite(LED_PIN, HIGH);
      analogWrite(rightREAR, 0);
      analogWrite(leftREAR, 0);

      analogWrite(rigthFRONT, speedRight);
      analogWrite(leftFRONT, speedLeft);
      delay(20);

      serialString = '0';
    }
    
    /*stop*/
    if (serialString == 'S')
    {
      digitalWrite(LED_PIN, LOW);
      analogWrite(rightREAR, 0);
      analogWrite(leftREAR, 0);
      analogWrite(rigthFRONT, 0);
      analogWrite(leftFRONT, 0);
      serialString = '0';
    }
  }
}

