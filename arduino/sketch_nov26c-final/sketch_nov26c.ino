#include <SoftwareSerial.h>

#define LED_PIN     13

static boolean light_led = false;
static unsigned long nextSwitchTime = millis() + 300000;

int leftFRONT  = 5;
int leftREAR   = 6;
int rigthFRONT = 9;
int rightREAR  = 10;

int i = 0;         // variable to store the read value
String serialString;
int speedSET = 0;
int speedLeft = 0;
int speedRight = 0;

SoftwareSerial mySerial(0, 1); // RX, TX

void setup() 
{
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  mySerial.begin(9600);

  pinMode(leftFRONT, OUTPUT);
  pinMode(leftREAR, OUTPUT);
  pinMode(rigthFRONT, OUTPUT);
  pinMode(rightREAR, OUTPUT);
}

void loop()
{
  // run over and over
  if (mySerial.available())
  {
    //delay(50);
    //serialString = mySerial.read();

    String first  = mySerial.readStringUntil('H');
    //mySerial.read(); //next character is comma, so skip it using this
    String second = mySerial.readStringUntil('A');
    //mySerial.read();
    String third  = mySerial.readStringUntil('A');
    //mySerial.read();
    String fourth  = mySerial.readStringUntil('T');
    
    /*move forward - receive a F and next two commands are left, then right speed settings for the PWM.
      All calculation for speed must be made on remote controler,
    */
    if (second == "S")
    {
      digitalWrite(LED_PIN, LOW);
      analogWrite(rightREAR, 0);
      analogWrite(leftREAR, 0);
      analogWrite(rigthFRONT, 0);
      analogWrite(leftFRONT, 0);
    }
    else if (second == "F")
    {
      digitalWrite(LED_PIN, HIGH);
      analogWrite(rightREAR, 0);
      analogWrite(leftREAR, 0);

      speedLeft=third.toInt();
      speedRight=fourth.toInt();

      analogWrite(rigthFRONT, speedRight);
      analogWrite(leftFRONT, speedLeft);
      
      //delay(20);
    }
    //serialString = '0';
  }
}

