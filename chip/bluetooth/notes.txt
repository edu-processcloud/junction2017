GetCHIP
=======
Created Saturday 15 July 2017

#### #Connect through USB serial cable
screen /dev/ttyACM1 115200

user: chip
pass: chip

##### #Get WIFI list
nmcli device wifi list


#### #setup wifi
sudo nmcli device wifi connect "Jukkis" password "jollejolle" ifname wlan0


##### #pins export
XIO-P7  sudo sh -c 'echo 1020 > /sys/class/gpio/export'
XIO-P6  sudo sh -c 'echo 1019 > /sys/class/gpio/export'
XIO-P5  sudo sh -c 'echo 1018 > /sys/class/gpio/export'
XIO-P4  sudo sh -c 'echo 1017 > /sys/class/gpio/export'
....

##### #to output
sudo sh -c 'echo out > /sys/class/gpio/gpio1020/direction'
sudo sh -c 'echo out > /sys/class/gpio/gpio1019/direction'
sudo sh -c 'echo out > /sys/class/gpio/gpio1018/direction'
sudo sh -c 'echo out > /sys/class/gpio/gpio1017/direction'

#### #ssh
ssh chip@10.100.35.168
172.20.10.2

#### #bluetooth
#Mac - A0:2C:36:44:3D:04
pair A0:2C:36:44:3D:04

#### #su -pchip
