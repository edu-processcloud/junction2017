#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <SoftwareSerial.h>


/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

#define led_pin     13
const int buttonPin1 = 5;     // the number of the pushbutton pin
const int buttonPin2 = 6;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status

SoftwareSerial portOne(0, 1);// RX, TX


/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (100)

Adafruit_BNO055 bno = Adafruit_BNO055();

int x, _x, xfactorL, xfactorR, xflag = 0;
int y, _y, ystatic, yflag = 0;
int z, _z, zstatic, zflag = 0;

void setup(void)
{

  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);

  portOne.begin(9600);
  portOne.listen();

  // initialize the LED pin as an output:
  pinMode(led_pin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);


  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
//    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
  //TODO blink error
    while(1);
  }

  delay(1000);
  bno.setExtCrystalUse(true);
}


void loop(void)
{

    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

    _x = euler.x();
    _y = euler.y();
    _z = euler.z();

    if( x != _x || y != _y || z != _z ){
        if( _y < -5 ){  //hands is down and should start to move

            if( _x > 300 ){
            //esta girando para esquerda
            //diminui aceleracao do motor esquerdo
                xfactorL = 60 - (_x - 300);
                xfactorR = 0;
            }

            if( (_x >= 0 ) && ( _x <= 60 )){
            //esta girando para direita
            //diminui aceleracao do motor esquerdo
                xfactorL = 0;
                xfactorR = _x;
            }

            if( (_x > 60 ) && ( _x <= 300 )){
            //esta fora do angulo de gira e baixa a velocidade
            //diminui aceleracao do motor esquerdo
                xfactorL = 30;
                xfactorR = 30;
            }

            // turn LED on:
            digitalWrite(led_pin, HIGH);
            portOne.write( 'F' );
            delay(14);
            portOne.write( (_y * -2) - xfactorL );    //first is left motor
            delay(14);
            portOne.write( (_y * -2) - xfactorR );    //second is right motor
            delay(14);
            digitalWrite(led_pin, LOW);
        }
        if( _y >= -5 ){
            // turn LED on:
            digitalWrite(led_pin, HIGH);
            portOne.write( 'S' );
            delay(10);
            digitalWrite(led_pin, LOW);
        }
    }
    x = euler.x();
    y = euler.y();
    z = euler.z();
  delay(BNO055_SAMPLERATE_DELAY_MS);
}
