package com.movesense.mds.sampleapp.example_app_using_mds_api.tests;

import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsSubscription;
import com.movesense.mds.sampleapp.BleManager;
import com.movesense.mds.sampleapp.ConnectionLostDialog;
import com.movesense.mds.sampleapp.R;
import com.movesense.mds.sampleapp.example_app_using_mds_api.FormatHelper;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.AngularVelocity;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.LinearAcceleration;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.MagneticField;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.MovesenseConnectedDevices;
import com.polidea.rxandroidble.RxBleDevice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

public class MultiSubscribeActivity extends AppCompatActivity implements BleManager.IBleConnectionMonitor {

    @BindView(R.id.switchSubscriptionLinearAcc)
    SwitchCompat switchSubscriptionLinearAcc;
    @BindView(R.id.x_axis_linearAcc_textView)
    TextView xAxisLinearAccTextView;
    @BindView(R.id.y_axis_linearAcc_textView)
    TextView yAxisLinearAccTextView;
    @BindView(R.id.z_axis_linearAcc_textView)
    TextView zAxisLinearAccTextView;

    @BindView(R.id.dataX)
    TextView _dataX;
    @BindView(R.id.dataY)
    TextView _dataY;
    //int flag = 1;

    @BindView(R.id.switchSubscriptionMagneticField)
    SwitchCompat switchSubscriptionMagneticField;
    @BindView(R.id.x_axis_magneticField_textView)
    TextView xAxisMagneticFieldTextView;
    @BindView(R.id.y_axis_magneticField_textView)
    TextView yAxisMagneticFieldTextView;
    @BindView(R.id.z_axis_magneticField_textView)
    TextView zAxisMagneticFieldTextView;
    @BindView(R.id.switchSubscriptionAngularVelocity)
    SwitchCompat switchSubscriptionAngularVelocity;
    @BindView(R.id.x_axis_angularVelocity_textView)
    TextView xAxisAngularVelocityTextView;
    @BindView(R.id.y_axis_angularVelocity_textView)
    TextView yAxisAngularVelocityTextView;
    @BindView(R.id.z_axis_angularVelocity_textView)
    TextView zAxisAngularVelocityTextView;

    private final String LOG_TAG = MultiSubscribeActivity.class.getSimpleName();
    private final String LINEAR_ACCELERATION_PATH = "Meas/Acc/";
    private final String MAGNETIC_FIELD_PATH = "Meas/Magn/";
    private final String ANGULAR_VELOCITY_PATH = "Meas/Gyro/";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    @BindView(R.id.connected_device_name_textView)
    TextView mConnectedDeviceNameTextView;
    @BindView(R.id.connected_device_swVersion_textView)
    TextView mConnectedDeviceSwVersionTextView;
    private MdsSubscription mdsSubscriptionLinearAcc;
    private MdsSubscription mdsSubscriptionMagneticField;
    private MdsSubscription mdsSubscriptionAngularVelocity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_subscribe);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Multi Subscribe");
        }

        mConnectedDeviceNameTextView.setText("Serial: " + MovesenseConnectedDevices.getConnectedDevice(0)
                .getSerial());

        mConnectedDeviceSwVersionTextView.setText("Sw version: " + MovesenseConnectedDevices.getConnectedDevice(0)
                .getSwVersion());

        BleManager.INSTANCE.addBleConnectionMonitorListener(this);



        final int sekundi = 0 ;
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            int sec = 0;
            @Override
            public void run() {
                    makeRequest2("http://172.20.10.2", _dataX.getText().toString(), _dataY.getText().toString());
                System.out.println("timer");
            }
        };
        timer.scheduleAtFixedRate(t,6000,5000);
//        timer.schedule(t,6000,2000);



    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

        BleManager.INSTANCE.removeBleConnectionMonitorListener(this);
    }

    @OnCheckedChanged(R.id.switchSubscriptionLinearAcc)
    public void onCheckedChangedLinear(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Log.d(LOG_TAG, "+++ Subscribe LinearAcc");
            mdsSubscriptionLinearAcc = Mds.builder().build(this).subscribe(URI_EVENTLISTENER,
                    FormatHelper.formatContractToJson(MovesenseConnectedDevices.getConnectedDevice(0)
                            .getSerial(), LINEAR_ACCELERATION_PATH + "13"), new MdsNotificationListener() {
                        @Override
                        public void onNotification(String data) {
                            Log.d(LOG_TAG, "onSuccess(): " + data);

                            LinearAcceleration linearAccelerationData = new Gson().fromJson(
                                    data, LinearAcceleration.class);

                            if (linearAccelerationData != null) {

                                LinearAcceleration.Array arrayData = linearAccelerationData.body.array[0];

                                xAxisLinearAccTextView.setText(String.format(Locale.getDefault(),
                                        "x: %.6f", arrayData.x));

                                yAxisLinearAccTextView.setText(String.format(Locale.getDefault(),
                                        "y: %.6f", arrayData.y));

                                zAxisLinearAccTextView.setText(String.format(Locale.getDefault(),
                                        "z: %.6f", arrayData.z));

                                //here is the place to send data to the chip





                             /*   makeRequest2("http://172.20.10.2", String.format(Locale.getDefault(),
                                        "%.1f", ((int)Math.round(arrayData.x) * 30)), String.format(Locale.getDefault(),
                                        "%.1f", (arrayData.y * 30)));
*/
//                                _dataX.setText(Integer.toString((int)Math.round((arrayData.x * 2) )));
  //                              _dataY.setText(Integer.toString((int)Math.round(Math.abs(arrayData.y * 15) )));

                                _dataX.setText(  Integer.toString((int)Math.round(Math.abs(arrayData.y * 15) )  -       (int)Math.round((arrayData.x * 2) )));
                                _dataY.setText(Integer.toString((int)Math.round(Math.abs(arrayData.y * 15) )   +  (int)Math.round((arrayData.x * 2) ))  );

                            }
                        }

                        @Override
                        public void onError(MdsException error) {
                            Log.e(LOG_TAG, "onError(): ", error);
                        }
                    });
        } else {
            Log.d(LOG_TAG, "--- Unsubscribe LinearAcc");
            mdsSubscriptionLinearAcc.unsubscribe();
        }
    }




    /* --------------------- */
    public static String makeRequest2(String uri ,String left ,String right) {
        HttpURLConnection conn;
        String result = null;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            //URL url = new URL(uri);
            //HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn = (HttpURLConnection) ((new URL(uri).openConnection()));

            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("command", "HFA" + left + "A" + right + "T");  //"HFA100A100T
//                .appendQueryParameter("command", "HF," +  left + "," + right + "T");  //"HF,100,100T
                String query = builder.build().getEncodedQuery();


            //Write
            OutputStream outputStream = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            new OutputStreamWriter(outputStream, "UTF-8");
            writer.write(query);
            writer.flush();
            writer.close();
            outputStream.close();
            conn.connect();


            //Read
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

            Log.d("post message", result);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    return "x";
}



    /*--------------------------------*/


    public static String makeRequest(String uri, String json) {
        HttpURLConnection urlConnection;
        String url;
        String data = json;
        String result = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);




        try {
            //Connect
            urlConnection = (HttpURLConnection) ((new URL(uri).openConnection()));
            urlConnection.setDoOutput(true);
            //urlConnection.setRequestProperty("Content-Type", "application/json");
            //urlConnection.setRequestProperty("Accept", "application/json");
            //urlConnection.setRequestMethod("POST");

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Connection", "Keep-Alive");
            urlConnection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            urlConnection.setRequestProperty("Content-Type", "multipart/form-data");

            urlConnection.connect();

            //Write
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(data);
            writer.close();
            outputStream.close();

            //Read
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

            Log.d("post message", result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }



    /*--------------------------------*/





    @OnCheckedChanged(R.id.switchSubscriptionMagneticField)
    public void onCheckedChangedMagnetic(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Log.d(LOG_TAG, "+++ Subscribe MagneticField");
            mdsSubscriptionMagneticField = Mds.builder().build(this).subscribe(URI_EVENTLISTENER,
                    FormatHelper.formatContractToJson(MovesenseConnectedDevices.getConnectedDevice(0)
                            .getSerial(), MAGNETIC_FIELD_PATH + "13"), new MdsNotificationListener() {
                        @Override
                        public void onNotification(String data) {
                            Log.d(LOG_TAG, "onSuccess(): " + data);

                            MagneticField magneticField = new Gson().fromJson(
                                    data, MagneticField.class);

                            if (magneticField != null) {

                                MagneticField.Array arrayData = magneticField.body.array[0];

                                xAxisMagneticFieldTextView.setText(String.format(Locale.getDefault(),
                                        "x: %.6f", arrayData.x));
                                yAxisMagneticFieldTextView.setText(String.format(Locale.getDefault(),
                                        "y: %.6f", arrayData.y));
                                zAxisMagneticFieldTextView.setText(String.format(Locale.getDefault(),
                                        "z: %.6f", arrayData.z));

                            }
                        }

                        @Override
                        public void onError(MdsException error) {
                            Log.e(LOG_TAG, "onError(): ", error);
                        }
                    });
        } else {
            Log.d(LOG_TAG, "--- Unsubscribe MagneticField");
            mdsSubscriptionMagneticField.unsubscribe();
        }
    }

    @OnCheckedChanged(R.id.switchSubscriptionAngularVelocity)
    public void onCheckedChangedAngularVielocity(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Log.d(LOG_TAG, "+++ Subscribe AngularVelocity");
            mdsSubscriptionAngularVelocity = Mds.builder().build(this).subscribe(URI_EVENTLISTENER,
                    FormatHelper.formatContractToJson(MovesenseConnectedDevices.getConnectedDevice(0)
                            .getSerial(), ANGULAR_VELOCITY_PATH + "13"), new MdsNotificationListener() {
                        @Override
                        public void onNotification(String data) {
                            Log.d(LOG_TAG, "onSuccess(): " + data);

                            AngularVelocity angularVelocity = new Gson().fromJson(
                                    data, AngularVelocity.class);

                            if (angularVelocity != null) {

                                AngularVelocity.Array arrayData = angularVelocity.body.array[0];

                                xAxisAngularVelocityTextView.setText(String.format(Locale.getDefault(),
                                        "x: %.6f", arrayData.x));
                                yAxisAngularVelocityTextView.setText(String.format(Locale.getDefault(),
                                        "y: %.6f", arrayData.y));
                                zAxisAngularVelocityTextView.setText(String.format(Locale.getDefault(),
                                        "z: %.6f", arrayData.z));

                            }
                        }

                        @Override
                        public void onError(MdsException error) {
                            Log.e(LOG_TAG, "onError(): ", error);
                        }
                    });
        } else {
            Log.d(LOG_TAG, "--- Unsubscribe AngularVelocity");
            mdsSubscriptionAngularVelocity.unsubscribe();
        }
    }

    @Override
    public void onDisconnect(RxBleDevice rxBleDevice) {
        Log.e(LOG_TAG, "onDisconnect: " + rxBleDevice.getName() + " " + rxBleDevice.getMacAddress());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConnectionLostDialog.INSTANCE.showDialog(MultiSubscribeActivity.this);
            }
        });
    }

    @Override
    public void onConnect(RxBleDevice rxBleDevice) {
        Log.e(LOG_TAG, "onConnect: " + rxBleDevice.getName() + " " + rxBleDevice.getMacAddress());
        ConnectionLostDialog.INSTANCE.dismissDialog();
    }
}
