#
# Be sure to run `pod lib lint Movesense.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Movesense'
  s.version          = '1.0.0'
  s.summary          = 'Library for communicating with Movesense-compatible devices over Bluetooth Low Energy'

  s.homepage         = 'http://www.movesense.com'
  s.license          = { :type => '', :file => 'LICENSE' }
  s.authors          = { 'Suunto' => 'suunto@suunto.com' }
  s.source           = { :git => 'ssh://git@altssh.bitbucket.org:443/suunto/movesense-mobile-lib.git' }

  s.ios.deployment_target = '10.0'
  s.library = 'stdc++', 'z'

  s.source_files = 'IOS/Movesense/include/*.h', 'IOS/Movesense/swift/*'
  s.vendored_library = 'IOS/Movesense/Release-iphoneos/libmds.a'
end
